rsync for Google Drive
======================


Google Drive is a poor replication of things filesystems could do 20 years ago, but in 🚀🙌🙌🤘the cloud🤘🙌🙌🚀.
It is pretty good for collaboration, not as good at managing massive databases.
In particular, it doesn't allow copying folders; probably Google's engineers decided
a) that would be hard b) it might put heavy load on their servers.

This program puts that heavy load on their servers.


Uses
----

### Migrating between between Google Suite Accounts

There are [many](TODO) [examples](TODO) of people complaining that there is no tool for migrating data into Google Drive.

Drive has a rule that you cannot change the ownership of Google Drive files across domains ("Sorry, cannot transfer ownership to user@example.org. Ownership can only be transferred to another user in the same domain as the current owner.").
What that means is that while you can transfer ownership of files from one @gmail.com account to another,
you cannot switch from one Google Suite account to another because they ar attached to different domain names,
and you cannot move files from personal Google accounts into a Google Suite account.

Trying to use Google's built in exporter often loses files or drops folder structure.

This tool can work around this by copying files instead of moving them.


### Migration into a Team Drive

Team Drives are a new, incomplete, feature which Google is rolling out.
Right not they have a rule that "Folders cannot currently be moved to Team Drives."
And since none of Google Drive allows folder copying, there is no reasonable way to move
a block of files into a Team Drive.

`google-drive-rsync` can get around this. The process is basically the same as before but
for you only need one user account involved.

### Migration into Google Drive

Currently unsupported, but not difficult to do so check back.


### Migration out of Google Drive

Currently unsupported, but not difficult to do so check back.


### Migration between two @gmail.com accounts

If you are _only_ attempting to transfer between two @gmail.com accounts you do not need this tool;
just change the ownership on your files.

(I believe the same should be true for transfering between any personal google account,
regardless of the actual domain name in the username, but haven't tested.)

Installation
------------

TODO

Usage
-----

```
google-drive-rsync [-u account] [-w] [-l] source_folder target_folder
```

`google-drive-rsync` runs a minature version of the rsync algorithm: it only copies source files that are newer than their corresponding targets.
This means you can be sure that the target folder always has the latest, and that syncing can be done multiple times.

The Google OAuth credentials, allowing read-only access to the user's profile, and read-write access to their Drive files,
are cached to `~/.cache/google-drive-rsync/account.json`, so be careful to protect this folder.


To use, first choose a Google account to handle the sync.  This account will be listed
as alternately the owner (for regular Drive files) or the creator (for [Team Drive](https://gsuite.google.com/learning-center/products/drive/get-started-team-drive/) files) of
the copy, so you probably want to pick the owner of the Drive you are migrating to, or make
a scrap account (e.g. legacy@yourdomain.com) if you're migrating into a Team Drive.
Call this user the migration user.

The migration user needs to have read access to the source folder and write access to the target folder.
The easiest way to arrange this is to login as the owner of the source folder and [Share...](https://gsuite.google.com/learning-center/products/drive/get-started-team-drive/#section-4)
it to the migration user.

Next, you need to find the URLs for the folders to copy between.

Go to https://drive.google.com, login as the migration user, and find the source and target folders.
The source folder will be somewhere under Shared With Me; if you cannot see the source folder as the
migration user, then `google-drive-rsync` won't be able to either.
The target could be, for example, the top level folder of your drive, a new target folder in it,
or an entire Team Drive.
Click them open and copy down the URLs for each.
For a migration you should make sure the target is empty before you start.
Double check which one is which; copying the target over the source would end badly.

![Finding Google Drive IDs](.docs/folder_ids.png "Finding Google Drive folder IDs")

Finally, start the migration like this:

```
google-drive-rsync -u legacy@yourdomain.com https://drive.google.com/drive/u/0/folders/0B6-FyOZbgYJFdDJRLTJ5dnNPbGs https://drive.google.com/drive/u/0/my-drive
```

The first time you run this you will be prompted to login to Google, and authorize `google-drive-rsync` to manage your files.

You can rerun this as many times as you need to to update the synchornization of the source and target folders.
For example, you can cronjob it over a week or a month in advance of a planned migration, so that the day of the migration
the files are already on the new account, and up to date with the old files.

Finally, once the migration is done, remove permissions to the old files by

1. Unsharing them from everyone
1. Disabling the Share Link
1. Changing the password on the owning account to lock everyone out

After a while, when everyone is comfortable using the new location and you are confident
there were not any drastic oversights during the copy, say a few months, you can follow this with

1. Deleting the old files for good


Known Issues
------------

Sometimes this trips bugs in Google's API and some files will fail to copy. In this case, the sync will skip them and move on.
You can just re-run the sync and it will be faster. 

Copying Google Maps files linked into Google Drive is buggy or maybe unsupported. This will fail to copy them.
There is a manual workaround: [manually download the .kml file and reupload it to the new account](https://sites.google.com/site/gmapstips/copy-move-features-between-my-maps).

--delete is unsupported, so make sure your target folder is empty before you start or else you will end up with extra files in the target.

Google identifies files by ID, not by filename, but this tool identifies files by ID.
If you have two files with the same name in a folder, only one will be copied.

Google allows plentiful use of hardlinking: a single file/folder can be contained in many other files/folders.
This program does not try to preserve this structure at all; files in multiple folders will be copied multiple times.

Running this with expired cached credentials is untested. If you run into this, tell us how it goes,
and then erase `~/.cache/google-drive-rsync`.


Related Work
------------

* [drive](https://github.com/odeke-em/drive), which behaves more like Dropbox;
  for syncing remote Google files to local files, rather than syncing two remote Google accounts.
* [migrationtool](http://migrationtool.sourceforge.net/) (for IMAP), which solves this problem for IMAP (but you can also just use drag-and-drop in Thunderbird)
