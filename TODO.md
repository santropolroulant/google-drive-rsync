
* google_api_python_client uses [oauth2lib](https://github.com/google/oauth2client) which has been _deprecated_ by Google (!!!) in favour of
  [oauthlib](https://oauthlib.readthedocs.io/), [google-auth](https://google-auth.readthedocs.io) 
* warn if multple files have the same name in a folder (actually, ...why)
* warn if a file/folder has multiple parents, because we do not support copying these
* automate downloading and reuploading .kml files to copy google maps, since it doesn't work: https://sites.google.com/site/gmapstips/copy-move-features-between-my-maps
